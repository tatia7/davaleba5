package com.example.davaleba5

import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.davaleba5.databinding.ActivityMainBinding
import org.w3c.dom.Text

class profileActivity : AppCompatActivity() {

    private lateinit var email1 : TextView
    private lateinit var firstname1 : TextView
    private lateinit var lastname1 : TextView
    private lateinit var age1 : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)


        init()
    }
    private fun init(){
        email1 = findViewById(R.id.email1)
        firstname1 = findViewById(R.id.firstname1)
        lastname1 = findViewById(R.id.lastname1)
        age1 = findViewById(R.id.age1)



        changeProfile()

    }
    private fun changeProfile(){
        var bundle: Bundle ?= intent.extras
        var email = bundle!!.getString("Email")
        var firsname = bundle!!.getString("FirstName")
        var lastname = bundle!!.getString("LastName")
        var age = bundle!!.getString("Age")

        email1.setText(email)
        firstname1.setText(firsname)
        lastname1.setText(lastname)
        age1.setText(age)

    }

}