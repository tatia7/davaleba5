package com.example.davaleba5

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import com.example.davaleba5.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val users = mutableMapOf<String, User>()
    private lateinit var succ_fail : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        init()


    }

    private fun init() {
        binding.addUser.setOnClickListener {
            if (check() == true) {
                add()
            }
        }
        binding.update.setOnClickListener {
            if (check() == true) {
                update()
            }
        }
        binding.remove.setOnClickListener {
            remove()
        }
        binding.newActive.setOnClickListener {
            newAct()
        }


    }
    private fun newAct() {
        val em : String = binding.email.text.toString()
        val fn : String = binding.firstname.text.toString()
        val ln : String = binding.lastname.text.toString()
        val g : String = binding.age.text.toString()
        val intent = Intent(this, profileActivity::class.java)
        intent.putExtra("Email", em)
        intent.putExtra("FirstName", fn)
        intent.putExtra("LastName", ln)
        intent.putExtra("Age", g)
        startActivity(intent)
    }

    private fun add(){
        val email = binding.email.text.toString()
        val firstName = binding.firstname.text.toString()
        val lastName = binding.lastname.text.toString()
        val age = binding.age.text.toString()

        users["default"] = User("email", "firstName", "lastName", "age")
        if (email in users.keys) {
            Toast.makeText(this, "not successful", Toast.LENGTH_SHORT).show()
            ///succ_fail.setText("Not Successful")
            ///succ_fail.setTextColor(Color.RED)
        } else {
            users[email] = User(email, firstName, lastName, age)
            ///succ_fail.setText("Successful")
            ///succ_fail.setTextColor(Color.GREEN)
            Toast.makeText(this, "successful", Toast.LENGTH_SHORT).show()
        }
    }

    private fun update()  {
        val email = binding.email.text.toString()
        val firstName = binding.firstname.text.toString()
        val lastName = binding.lastname.text.toString()
        val age = binding.age.text.toString()

        val updatedUser = User(email, firstName, lastName, age)

        if (email in users.keys) {
            users.put(email, updatedUser)
            Toast.makeText(this, "updated successfully", Toast.LENGTH_SHORT).show()
            ///succ_fail.setText("Successful")
            ///succ_fail.setTextColor(Color.GREEN)
        } else {
            Toast.makeText(this, "user doesnt exist", Toast.LENGTH_SHORT).show()
            ///succ_fail.setText("Not Successful")
            ///succ_fail.setTextColor(Color.RED)
        }
    }

    private fun remove() {
        val email = binding.email.text.toString()

        if (email in users.keys) {
            users.remove(email)
            Toast.makeText(this, "successful remove", Toast.LENGTH_SHORT).show()
            ///succ_fail.setText("Successful")
            ///succ_fail.setTextColor(Color.GREEN)
        } else {
            Toast.makeText(this, "user doesnt exist", Toast.LENGTH_SHORT).show()
            ///succ_fail.setText("Not Successful")
            ///succ_fail.setTextColor(Color.RED)
        }
    }



    private fun check(): Boolean {
        val email = binding.email.text.toString()
        val firstName = binding.firstname.text.toString()
        val lastName = binding.lastname.text.toString()
        val age = binding.age.text.toString()

        if (email.isEmpty() or firstName.isEmpty() or lastName.isEmpty() or age.isEmpty()){
            Toast.makeText(this, "fill", Toast.LENGTH_SHORT).show()
            return false
        } else if("@" !in email){
            Toast.makeText(this, "Email not good", Toast.LENGTH_SHORT).show()
            return false
        }else if (age.toInt() < 0) {
            Toast.makeText(this, "Not good age", Toast.LENGTH_SHORT).show()
            return false
        }else{
            return true
        }

    }
}