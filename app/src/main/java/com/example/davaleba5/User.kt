package com.example.davaleba5

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(var email : String, var firstname : String, var lastname : String, var age : String) : Parcelable {
    override fun describeContents(): Int {
        TODO("Not yet implemented")
    }
}